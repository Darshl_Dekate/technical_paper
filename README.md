## **Representational** **State** **Transfer** **(REST)** 
This term “REST” was first defined by Roy Fielding in 2000. REST architectural style is a worldview that elevates information into a first-class element of architectures . REST allows us to achieve the architectural properties of performance, scalability, generality, simplicity, modifiability, and extensibility.
REST allows requesting systems to access and manipulate web resources by using a uniform and predefined set of rules. Interaction in REST based systems happen through Internet’s Hypertext Transfer Protocol (HTTP).
Restful system consists of a two factor one is client who requests for the resources and another is server who has the resources . 
<br />
## Architectural Constraints of RESTful API :
There are six architectural constraints which makes any web service are listed below:

### 1.Uniform Interface<br />
### 2.Stateless<br />
### 3.Cacheable<br />
### 4.Client-Server<br />
### 5.Layered System<br />
### 6.Code on Demand<br />
![REStful API](https://raw.githubusercontent.com/Codecademy/articles/0b631b51723fbb3cc652ef5f009082aa71916e63/images/rest_api.svg)
<br />
The only optional constraint of REST architecture is code on demand. If a service violates any other constraint, it cannot strictly be referred to as RESTful.

## Uniform Interface: 
It is a key constraint that differentiate between a REST API and Non-REST API. It suggests that there should be an uniform way of interacting with a given server irrespective of device or type of application (website, mobile app).
There are four guidelines principle of Uniform Interface are:
<br />

## Resource-Based: Individual resources are identified in requests. For example: API/users.
Manipulation of Resources Through Representations: Client has representation of resource and it contains enough information to modify or delete the resource on the server, provided it has permission to do so. Example: Usually user get a user id when user request for a list of users and then use that id to delete or modify that particular user.
<br />

## Self-descriptive Messages: Each message includes enough information to describe how to process the message so that server can easily analyses the request.
Hypermedia as the Engine of Application State (HATEOAS): It need to include links for each response so that client can discover other resources easily.
<br />

## Stateless: 
It means that the necessary state to handle the request is contained within the request itself and server would not store anything related to the session. In REST, the client must include all information for the server to fulfill the request whether as a part of query params, headers or URI. Statelessness enables greater availability since the server does not have to maintain, update or communicate that session state. There is a drawback when the client need to send too much data to the server so it reduces the scope of network optimization and requires more bandwidth.
<br />

##  Cacheable: 
Every response should include whether the response is cacheable or not and for how much duration responses can be cached at the client side. Client will return the data from its cache for any subsequent request and there would be no need to send the request again to the server. A well-managed caching partially or completely eliminates some client–server interactions, further improving availability and performance. But sometime there are chances that user may receive stale data.
<br />

## Client-Server: 
REST application should have a client-server architecture. A Client is someone who is requesting resources and are not concerned with data storage, which remains internal to each server, and server is someone who holds the resources and are not concerned with the user interface or user state. They can evolve independently. Client doesn’t need to know anything about business logic and server doesn’t need to know anything about frontend UI.
<br />

## Layered system: 
An application architecture needs to be composed of multiple layers. Each layer doesn’t know any thing about any layer other than that of immediate layer and there can be lot of intermediate servers between client and the end server. Intermediary servers may improve system availability by enabling load-balancing and by providing shared caches.
<br />

##  Code on demand: 
It is an optional feature. According to this, servers can also provide executable code to the client. The examples of code on demand may include the compiled components such as Java applets and client-side scripts such as JavaScript.
<br />

## References
https://www.geeksforgeeks.org/rest-api-architectural-constraints/<br />

https://www.codecademy.com/article/what-is-rest<br />

https://www.webscrapingapi.com/rest-api-architecture-constraints/<br />

https://youtu.be/qVTAB8Z2VmA<br />